///Created by Lucas Jakober
///Created October 21, 2015
///An instance of Movement that implements the strategy pattern and specifies one operation; path()


#ifndef ROOKMOVEMENT_H
#define ROOKMOVEMENT_H

#include <vector>
#include "Movement.h"


class RookMovement : public Movement
{
public:
	RookMovement();

	std::vector<Coord*> path(Coord* start, Coord* end);

};


#endif 