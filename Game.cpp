///Created by Lucas Jakober
///Created September 28, 2015
///An abstraction of a board game.  The game has a Board and relies on 
///virtual functions being defined in specific game types.

#include "Game.h"


///Calls on Board Class methods to execute the Game.  
///Note: ALL VIRTUAL methods defined in sub-classes of Game
const void Game::play()
{
///Sets up the specific game
   setup();
   ///Outputs the starting Board before any moves have been made
	gameBoard->draw(&cout);

	///Loops until isOver() method returns true indicating the game has ended.
	///In each loop one piece is moved from a valid starting coordinate(firstLoc) to
	///a valid destination coordinate(secondLoc).  The result is then output for each 
	///move in the draw() method. int i is the counter representing whose turn it is.
   int i = 2;
   while(!isOver())
   {  
         playerTurn(i);

         std::cout << "Enter the row number <space> column number, of the piece you wish to move" << std::endl;
         Square* firstLoc = getSquare(std::cin);
         
   		while(!validFirstLocation(i, c))
         {
            firstLoc = getSquare(std::cin);
         }

         Coord* start = new Coord();
         start->row = c->row;
         start->column = c->column;
         std::cout<< start->row << "  " << start->column << std::endl;

         std::cout << "Enter the destination coordinates" << std::endl;
         Square* secondLoc = getSquare(std::cin);

         Coord* end = new Coord();
         end->row = c->row;
         end->column = c->column;

///possibleMoves is a vector containing in order, the valid coordinates connecting a start location
///and an end location on an empty board, as determined by the instance of path(start, end) that is called.
///This vector will be checked later on by legal(possibleMoves, firstLoc) to determine if a legal move
///exists in the vector, considering the location of all other pieces on the board.
         vector<Coord*> possibleMoves = firstLoc->getPiece()->move->path(start, end);
         Coord* temp = new Coord();
         temp = legal(possibleMoves,firstLoc); 

///If possibleMoves did not contain a legal move, the user must re-select a starting and ending location for a piece
///until the move is legal, at which point the move will be executed.
         while(temp == NULL)
         {
            std::cout << "That move was invalid. Re-enter your start and end coordinates." << std::endl;
            firstLoc = getSquare(std::cin);
            start->row = c->row;
            start->column = c->column;
            secondLoc = getSquare(std::cin);
            end->row = c->row;
            end->column = c->column;
            possibleMoves = firstLoc->getPiece()->move->path(start, end);

            temp = legal(possibleMoves, firstLoc);

         }
///Execute the move of a piece and draw the result.  Delete the allocated memory.
         gameBoard->movePiece(firstLoc, secondLoc);
         gameBoard->draw(&cout);
         std::cout << std::endl;
         delete start;
         delete end;
         delete temp;         
         i++;
   }
///Output the winner.
   winner(i);

 }

///Outputs to the screen the winner of the game, based on which player made the final move.
void Game::winner(int i)
{
   if(i%2 == 0)
   {
      std::cout << "Player two WINS!" << std::endl;
   }
   if(i%2 == 1)
   {
      std::cout << "Player one WINS!" << std::endl;
   }
}

///Outputs to the screen which player's turn it is.
void Game::playerTurn(int i)
{
   if(i%2 == 0)
   {
      std::cout << "Player one, it's your turn." << std::endl;
   }
   if(i%2 == 1)
   {
      std::cout << "Player two, it's your turn." << std::endl;
   }
}

///Virtual destructor to be defined in sub classes
Game::~Game() {}






