///Created by Lucas Jakober
///Created September 28, 2015
///A class that provides the implementation of the abstract/virtual class of Game.
///It contains the Pieces of the game and inherits a Board member from Game.


#ifndef CHESS_H
#define CHESS_H


#include "Game.h"



class Chess : public Game
{

  public:
///Pieces of the Chess Game:
///first letter indicates color: 'w' = white, 'b' = black
///second letter indicates piece type: 'r' = rook, 'b' = bishop, 'p' = pawn, etc.
///number is used to differentiate between specific pieces of similar type (ie. bp1 and bp2)

  	Piece* wr1;
  	Piece* wb1;
  	Piece* wk1;
  	Piece* wq1;
  	Piece* wb2;
  	Piece* wr2;

  	Piece* wp1;
  	Piece* wp2;
  	Piece* wp3;
  	Piece* wp4;
  	Piece* wp5;
  	Piece* wp6;

  	Piece* br1;
  	Piece* bb1;
  	Piece* bk1;
  	Piece* bq1;
  	Piece* bb2;
  	Piece* br2;

  	Piece* bp1;
  	Piece* bp2;
  	Piece* bp3;
  	Piece* bp4;
  	Piece* bp5;
  	Piece* bp6;


	 Chess();
    ~Chess();
///Definition of virtual function setUp() inherited from Game
   	void setup();

///Definition of virtual function isOver() inherited from Game
   	bool isOver();

///Definition of virtual function getSquare() inherited from Game
	 Square* getSquare(std::istream &is);

///Returns true if the first location is valid
   bool validFirstLocation(int i, Coord* c);

///Takes a vector of coordinates and returns a null pointer unless a legal move
///exists in the vector.
   Coord* legal(std::vector<Coord*> v, Square* s);

};

#endif 