///Created by Lucas Jakober
///Created October 21, 2015
///An instance of Movement that implements the strategy pattern and specifies one operation; path()


#include "BlackPawnMovement.h"
#include <vector>
#include <cmath>
#include <iostream>


BlackPawnMovement::BlackPawnMovement(){}


std::vector<Coord*> BlackPawnMovement::path(Coord* start, Coord* end)
{
	std::vector<Coord*> v;
	int rowDiff = (end->row - start->row);
	int colDiff = (end->column - start->column);

	int rowCount = start->row;
	int colCount = start->column; 

///Handles vertical movement
///Ensures that a Black Pawn can only move square in the direction of the opposing player	if(colDiff == 0)
	if(colDiff == 0)
	{
		if(rowDiff == -1)
		{
			for(int i = 0; i < abs(rowDiff); i++)
			{
				Coord* c = new Coord(rowCount-1, colCount);
				v.push_back(c);
				rowCount--;
			}
		}
		return v;

	}
	else
		return v;

}

