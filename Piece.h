///Created by Lucas Jakober
///Created September 28, 2015
///A class that is an abstraction of a piece used in a board game.


#ifndef PIECE_H
#define PIECE_H

#include <string>

#include "Movement.h"



class Piece
{
  public:
///Pieces have a color to indicate the player and also a symbol
///to differentiate between Pieces 	
   std::string color;
   char symbol;
///Pieces have a pointer to a particular instance of movement, which is unique to their symbol and color;
   Movement* move;

  public:
///Constructor initializes the Piece's components
  	Piece(std::string c, char sym);


  	~Piece();

///Returns true if the Piece is still active on the Board.
   bool isAlive();

///Sets the Piece to be inactive on the Board
   void kill();

};


#endif