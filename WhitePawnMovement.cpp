///Created by Lucas Jakober
///Created October 21, 2015
///An instance of Movement that implements the strategy pattern and specifies one operation; path()

#include "WhitePawnMovement.h"


#include <vector>
#include <cmath>
#include <iostream>



WhitePawnMovement::WhitePawnMovement(){}



std::vector<Coord*> WhitePawnMovement::path(Coord* start, Coord* end)
{
	std::vector<Coord*> v;
	int rowDiff = (end->row - start->row);
	int colDiff = (end->column - start->column);

	int rowCount = start->row;
	int colCount = start->column; 

///Ensures that a White Pawn can only move square in the direction of the opposing player
	if(rowDiff < 1 || rowDiff > 1)
		return v;

///Handles vertical movement
	if(colDiff == 0)
	{
		if(rowDiff > 0 && rowDiff < 2)
		{
			for(int i = 0; i < rowDiff; i++)
			{
				Coord* c = new Coord(rowCount+1, colCount);
				v.push_back(c);
				rowCount++;
			}
		}
	}
	return v;
}


