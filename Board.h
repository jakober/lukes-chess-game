///Created by Lucas Jakober
///Created September 28, 2015
///An abstraction of a two-dimensional board that is made of Squares.


#ifndef BOARD_H
#define BOARD_H

#include "Square.h"
#include "Coord.h"


#include <ostream>
#include <vector>
#include <iostream>


using namespace std;

class Board 
{
  public:

///Dimensions of the Board
   int width, height;


///Board represented as a vector
	vector<vector<Square*>> grid;

  public:
   Board(int w, int h);

   ~Board();

///Outputs the board and each Square's symbol
   const void draw(ostream *o);

///Places a Piece on a specific Square within the Board
   void placePiece(Piece* p, Square* s);

///Moves a Piece if it exists from one Square to another Square
   void movePiece(Square* s, Square* d);

///Returns the Square at a given location on the Board
    Square* getSquare(Coord* c);

///Search the Board for a specific Piece to see if it exists on a Square
   Square* searchBoard(Piece* p);



	
};

#endif 