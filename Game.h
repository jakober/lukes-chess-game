///Created by Lucas Jakober
///Created September 28, 2015
///An abstraction of a board game.  The game has a Board and relies on 
///virtual functions being defined in specific game types.
#ifndef GAME_H
#define GAME_H

#include "Board.h"

#include <iostream>

class Game 
{

  public:
///The Board used by the Game
  	Board* gameBoard;
  	Coord* c;

///A method to play/execute the Game according the subclass definitions
///of setup(), isOVer(), and getSquare()
   const void play();

///Outputs to the screen the winner of the game, based on which player made the final move.
   void winner(int i);

///Outputs to the screen which player's turn it is.  
   void playerTurn(int i);

///To be specifically defined in any subClasses
   virtual bool validFirstLocation(int i, Coord* c) = 0;
   
///To be specifically defined in any subClasses
   virtual Coord* legal(std::vector<Coord*> v, Square* s) = 0;

///To be specifically defined in any subClasses
   virtual void setup() = 0;

///To be specifically defined in any subClasses
   virtual bool isOver() = 0;

///To be specifically defined in any subClasses
   virtual Square* getSquare(std::istream &is) = 0;

///To be specifically defined in any subClasses
   virtual ~Game() = 0;

};

#endif 


