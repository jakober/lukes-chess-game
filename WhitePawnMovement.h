///Created by Lucas Jakober
///Created October 21, 2015
///An instance of Movement that implements the strategy pattern and specifies one operation; path()

#ifndef WHITEPAWNMOVEMENT_H
#define WHITEPAWNMOVEMENT_H

#include <vector>
#include "Movement.h"




class WhitePawnMovement : public Movement
{
public:
	WhitePawnMovement();

	std::vector<Coord*> path(Coord* start, Coord* end);

};



#endif 