///Created by Lucas Jakober
///Created October 21, 2015
///A class used by Boards and instances of Movement to identify locations.


#ifndef COORD_H
#define COORD_H



class Coord {
public:
	int row;
	int column;

	Coord();
	Coord(int r, int c);

///Checks that the Coord object has coordinates that are in fact, on the Board.
	bool onBoard();

	~Coord();
};




#endif