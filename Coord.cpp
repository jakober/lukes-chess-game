///Created by Lucas Jakober
///Created October 21, 2015
///A class used by Boards and instances of Movement to identify locations

#include "Coord.h"

Coord::Coord() {
}

Coord::Coord(int r, int c)
{
	row = r;
	column = c;
}

///Retrns true if the object is has valid row and column to be within the Board parameters.
bool Coord::onBoard(){
	if(row < 0 || 5 < row)
		return false;
	if(column < 0 || 5 < column)
		return false;
	return true;
}

Coord::~Coord(){}


