///Created by Lucas Jakober
///Created October 21, 2015
///An interface that uses the strategy pattern and specifies one operation; path() 

#ifndef MOVEMENT_H
#define MOVEMENT_H

#include <vector>
#include "Coord.h"




class Movement {
public:
///To be specifically defined in any subClasses
	virtual std::vector<Coord*> path(Coord* start, Coord* end) = 0;
};



#endif 