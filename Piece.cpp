///Created by Lucas Jakober
///Created September 28, 2015
///A class that is an abstraction of a piece used in a board game.


#include "Piece.h"

#include "BlackPawnMovement.h"
#include "WhitePawnMovement.h"
#include "RookMovement.h"
#include "QueenMovement.h"
#include "BishopMovement.h"
#include "KingMovement.h"


Piece::Piece(std::string c, char sym){
	color = c;
	symbol = sym;

///Initializes the movement* move to the particular instance of movemment that applies
///to that piece.
	if(symbol == 'r' || symbol == 'R')
	   move = new RookMovement();

   else if(symbol == 'b' || symbol == 'B')
      move = new BishopMovement();

   else if(symbol == 'k' || symbol == 'K')
      move = new KingMovement();

   else if((symbol == 'p') && (color == "white"))
		move = new WhitePawnMovement();

   else if((symbol == 'P') && (color == "black"))
      move = new BlackPawnMovement();

   else
      move = new QueenMovement();
}

Piece::~Piece()
{
   delete move;
}


///Returns true if Piece is still active on Board.
///That is, if the symbol is not '.' which indicates it is inactive
   bool Piece::isAlive() {
   	if (symbol != '.' || NULL)
   		return true;
   	return false;
   }

///Sets the Piece to be inactive on the Board by setting symbol = '.'
///and setting color to "EMPTY"
   void Piece::kill() {
   		symbol = '.';
   		color = "EMPTY";
   }