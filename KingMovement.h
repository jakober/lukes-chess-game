///Created by Lucas Jakober
///Created October 21, 2015
///An instance of Movement that implements the strategy pattern and specifies one operation; path()


#ifndef KINGMOVEMENT_H
#define KINGMOVEMENT_H

#include <vector>
#include "Movement.h"


class KingMovement : public Movement
{
public:
	KingMovement();


	std::vector<Coord*> path(Coord* start, Coord* end);

};



#endif 