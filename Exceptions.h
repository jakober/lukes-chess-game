#ifndef EXCEPTIONS_H
#define EXCEPTIONS_H

#include <stdexcept>
#include <iostream>

///NOTE: I know how to use exceptions, however my program compiles and runs nicely without the use of try/catch, and so 
///I did not include them in my program.  That being said are ready for use in the future for possible software additions.

using std::runtime_error;

class invalid_coordinates_error : public runtime_error
{
public:
	invalid_coordinates_error():runtime_error("Invalid coordinates, please enter (x,y) such that 0 <= x <=5, 0 <= y <= 5."){}

	invalid_coordinates_error(std::string msg):runtime_error(msg.c_str()){}
};

class invalid_format_error : public runtime_error
{
public:
	invalid_format_error():runtime_error("You cannot enter a string or a character as a coordinate."){}
	invalid_format_error(std::string msg):runtime_error(msg.c_str()){}
};

class invalid_move_error : public runtime_error
{
public:
	invalid_move_error():runtime_error("Illegal move."){}
	invalid_move_error(std::string msg):runtime_error(msg.c_str()){}
};

class invalid_piece_error : public runtime_error
{
public:
	invalid_piece_error():runtime_error("No piece exists or that is the wrong player's piece."){}
	invalid_piece_error(std::string msg):runtime_error(msg.c_str()){}
};


#endif 