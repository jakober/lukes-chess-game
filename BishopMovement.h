///Created by Lucas Jakober
///Created October 21, 2015
///An instance of Movement that implements the strategy pattern and specifies one operation; path()


#ifndef BISHOPMOVEMENT_H
#define BISHOPMOVEMENT_H

#include <vector>
#include "Movement.h"
//#include "Piece.h"

class BishopMovement : public Movement
{
public:
	BishopMovement();


	std::vector<Coord*> path(Coord* start, Coord* end);

};



#endif 