///Created by Lucas Jakober
///Created September 28, 2015
///An abstraction of a Square on a Board. A location on a board
///that can contain a Piece.


#ifndef SQUARE_H
#define SQUARE_H

#include "Piece.h"



class Square 
{


  public:
///A pointer to Piece 
  	Piece* squarePiece;

///Unique coordinates for the Square's lcoation on the Board 
  	int row;
  	int column;

///Constructor that initializes the Square to a specific row and column 
///and also to be empty, in that it does not point to a Piece.
   Square(int r, int c) : row(r), column(c) {
   };


///Method for retrieving the symbol of the Piece on the Square
   const char symbol();

///Sets a Piece 'p' on the Square
   void setPiece(Piece* p);

///Returns the Piece that is on the Square
   Piece* getPiece();

///Removes the Piece from the Square
   Piece* removePiece();
};

#endif
