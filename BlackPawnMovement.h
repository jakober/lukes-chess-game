///Created by Lucas Jakober
///Created October 21, 2015
///An instance of Movement that implements the strategy pattern and specifies one operation; path()


#ifndef BLACKPAWNMOVEMENT_H
#define BLACKPAWNMOVEMENT_H

#include <vector>
#include "Movement.h"


class BlackPawnMovement : public Movement
{
public:
	BlackPawnMovement();

	std::vector<Coord*> path(Coord* start, Coord* end);

};



#endif 