///Created by Lucas Jakober
///Created October 21, 2015
///An instance of Movement that implements the strategy pattern and specifies one operation; path()

#include "RookMovement.h"
#include <vector>
#include <cmath>
#include <iostream>

RookMovement::RookMovement(){}


std::vector<Coord*> RookMovement::path(Coord* start, Coord* end)
{
	std::vector<Coord*> v;
	int rowDiff = (end->row - start->row);
	int colDiff = (end->column - start->column);

	int rowCount = start->row;
	int colCount = start->column;


///HANDLE HORIZONTAL MOVEMENT

	if(rowDiff == 0)
	{
		if(colDiff > 0)
		{
			for(int i = 0; i < colDiff; i++)
			{
				Coord* c = new Coord(rowCount, colCount+1);
				v.push_back(c);
				colCount++;
			}
		}
		if(colDiff < 0)
		{
			for(int i = (start->column -1); i > (end->column -1); i--)
			{
				Coord* c = new Coord(start->row, i);
				v.push_back(c);
			}
		}
	}

///HANDLE VERTICAL MOVEMENT

	if(colDiff == 0)
	{
		if(rowDiff > 0)
		{
			for(int i = 0; i < rowDiff; i++)
			{
				Coord* c = new Coord(rowCount+1, colCount);
				v.push_back(c);
				rowCount++;
			}
		}
		if(rowDiff < 0)
		{
			for(int i = 0; i< abs(rowDiff); i++)
			{
				Coord* c = new Coord(rowCount-1, colCount);
				v.push_back(c);
				rowCount--;
			}
		}
	}

	return v;
}