///Created by Lucas Jakober
///Created September 28, 2015
///An abstraction of a two-dimensional board that is made of Squares.

#include "Board.h"


#include <vector>
#include <assert.h>

using namespace std;

///Constructs a Board with dimensions Width and Height
Board::Board(int w, int h) : width(w), height(h), grid(height,vector<Square*>(width))
{
	///Traverses the Board and populates each location with a pointer to a Square
	for (vector<vector<Square*>>::size_type i = 0; i < grid.size(); i++)
	{
		for (vector<Square*>::size_type j = 0; j < grid[i].size(); j++)
		{
			grid[i][j] = new Square(i, j);
		}
	}
}

///Traverse the Board and deallocate the memory that was allocated for each pointer to a square
Board::~Board()
{
	for (vector<vector<Square*>>::size_type i = 0; i < grid.size(); i++)
	{
		for (vector<Square*>::size_type j = 0; j < grid[i].size(); j++)
		{
			delete grid[i][j];
		}
	}	
}

///This function is currently tailored to a specific Chess type Game. 
///Every Square on the Board is output starting with coordinate (0,0) 
///and proceeding to output (0, 1), (0,2) etc.
const void Board::draw(ostream *o)
 {
    *o << "       0     1     2     3     4     5   " << endl;
    *o << "     ___________________________________" << endl;
    *o << "    |     |     |     |     |     |     |" << endl;
 	for ( vector<vector<Square*>>::size_type i = 0; i < grid.size(); i++ )
	{
	   *o << " " << i << "  |  ";

   		for ( vector<Square*>::size_type j = 0; j < grid[i].size(); j++ )
   		{	
		   *o << grid[i][j]->symbol() << "  |  ";
   		}
   		*o << endl;
   		*o << "    |_____|_____|_____|_____|_____|_____|" << endl;
   		if(i<(grid.size()-1))
   		{
   			*o << "    |     |     |     |     |     |     |" << endl;
   		}
	}
}


///Assign the Piece pointer of a given Square to point to a given Piece
void Board::placePiece(Piece* p, Square* s) 
{
	s->squarePiece = p;

}

///Calls on Square methods to move a Piece located at Square 's' to Square 'd'
void Board::movePiece(Square* s, Square* d) { 
	assert(("No piece exists there", s->squarePiece!=NULL));
	if(d->getPiece() == NULL || d->getPiece()->symbol == '.' )
	{
		d->setPiece(s->getPiece());
		s->removePiece();
	}
	else if(d->getPiece() != NULL || d->getPiece()->symbol != '.')
	{
		///Piece* temp = d->getPiece();
		cout << endl << d->getPiece()->color << " " << d->getPiece()->symbol << " KILLED!" << endl << endl;
		d->setPiece(s->getPiece());
		s->removePiece();
	}

}

///Return the Square on the Board at the given Coord object c.
///Coordinates are of the form (row,column) on the board
Square* Board::getSquare(Coord* c) {
	return grid[c->row][c->column];
}


///Traverses each Square on the Board, similar to the draw() method
///looking for a specific piece.
Square* Board::searchBoard(Piece* p)
{
	for ( vector<vector<Square*>>::size_type i = 0; i < grid.size(); i++ )
	{
   		for ( vector<Square*>::size_type j = 0; j < grid[i].size(); j++ )
   		{	
      		if(grid[i][j]->getPiece() == p)
      			return grid[i][j];
      	}
    }
    return NULL;
}

