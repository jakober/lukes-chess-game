///Created by Lucas Jakober
///Created September 28, 2015
///An abstraction of a Square on a Board. A location on a board
///that can contain a Piece.


#include "Square.h"


///Returns the symbol of the Piece on the Square if it exists, otherwise
///it returns the symbol '.' indicating that the Square is empty
   const char Square::symbol(){
      if(squarePiece!=NULL)
   	{
        return squarePiece->symbol;
      }
      return '.';
   }
   

   
///Simply sets the Square's pointer to point to a Piece
   void Square::setPiece(Piece* p) {
      squarePiece = p;
   }

///Returns the pointer to the Piece on the Square
   Piece* Square::getPiece(){
   	return squarePiece;
   }

///Sets the Square to be empty. Sets the Piece pointer to equal NULL.
   Piece* Square::removePiece(){
         squarePiece = NULL;
      return squarePiece;
   }
