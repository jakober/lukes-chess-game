///Created by Lucas Jakober
///Created October 21, 2015
///An instance of Movement that implements the strategy pattern and specifies one operation; path()

#ifndef QUEENMOVEMENT_H
#define QUEENMOVEMENT_H

#include <vector>
#include "Movement.h"





class QueenMovement : public Movement
{
public:
	QueenMovement();


	std::vector<Coord*> path(Coord* start, Coord* end);

};



#endif 