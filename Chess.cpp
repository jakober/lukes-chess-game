///Created by Lucas Jakober
///Created September 28, 2015
///A class that provides the implementation of the abstract/virtual class of Game.
///It contains the Pieces of the game and inherits a Board member from Game.
#include "Chess.h"
#include <iostream>
#include <assert.h>
#include "Exceptions.h"

///Constructor initializes all the Pieces specific to Chess and 
///the specific Board for Chess
Chess::Chess(){
///Create the gameBoard specific to Chess (6x6)
	gameBoard = new Board(6,6);

	c = new Coord();
		
/// Create the white pieces - symbol in output will be a lower case
	wr1 = new Piece("white", 'r');
	wb1 = new Piece("white", 'b');
	wk1 = new Piece("white", 'k');
	wq1 = new Piece("white", 'q');
	wb2 = new Piece("white", 'b');
	wr2 = new Piece("white", 'r');

	wp1 = new Piece("white", 'p');
	wp2 = new Piece("white", 'p');
	wp3 = new Piece("white", 'p');
	wp4 = new Piece("white", 'p');
	wp5 = new Piece("white", 'p');
	wp6 = new Piece("white", 'p');

///Create the BLACK PIECES - symbol in output will be an upper case
	br1 = new Piece("black", 'R');
	bb1 = new Piece("black", 'B');
	bk1 = new Piece("black", 'K');
	bq1 = new Piece("black", 'Q');
    bb2 = new Piece("black", 'B');
	br2 = new Piece("black", 'R');

	bp1 = new Piece("black", 'P');
	bp2 = new Piece("black", 'P');
	bp3 = new Piece("black", 'P');	
	bp4 = new Piece("black", 'P');
	bp5 = new Piece("black", 'P');
	bp6 = new Piece("black", 'P');
	
}


///The definition provided for Game's virtual method setup()
///This instance of setup() places all of the Chess Pieces
///on the gameBoard to prepare Chess object for play() method
void Chess::setup() {

///Set the white pieces properly on one end of the gameBoard
	gameBoard->grid[0][0]->setPiece(wr1);
	gameBoard->grid[0][1]->setPiece(wb1);
	gameBoard->grid[0][2]->setPiece(wk1);
	gameBoard->grid[0][3]->setPiece(wq1);
	gameBoard->grid[0][4]->setPiece(wb2);
	gameBoard->grid[0][5]->setPiece(wr2);

	gameBoard->grid[1][0]->setPiece(wp1);
	gameBoard->grid[1][1]->setPiece(wp2);
	gameBoard->grid[1][2]->setPiece(wp3);
	gameBoard->grid[1][3]->setPiece(wp4);
	gameBoard->grid[1][4]->setPiece(wp5);
	gameBoard->grid[1][5]->setPiece(wp6);

///Set the black pieces properly on the other end of the gameBoard
	gameBoard->grid[5][0]->setPiece(br1);
	gameBoard->grid[5][1]->setPiece(bb1);
	gameBoard->grid[5][2]->setPiece(bk1);
	gameBoard->grid[5][3]->setPiece(bq1);
	gameBoard->grid[5][4]->setPiece(bb2);
	gameBoard->grid[5][5]->setPiece(br2);

	gameBoard->grid[4][0]->setPiece(bp1);
	gameBoard->grid[4][1]->setPiece(bp2);
	gameBoard->grid[4][2]->setPiece(bp3);
	gameBoard->grid[4][3]->setPiece(bp4);
	gameBoard->grid[4][4]->setPiece(bp5);
	gameBoard->grid[4][5]->setPiece(bp6);

}


///Definition of virtual Game method isOver(). Returns true if either King
///for black or white is no longer being pointed to by a Square on the gameBoard.
///That is, one of the Kings has been killed.  This method uses the searchBoard()
///method of class Board to determine if both Kings still remain on the gameBoard.
bool Chess::isOver() {
	if(gameBoard->searchBoard(wk1) == NULL || gameBoard->searchBoard(bk1) == NULL)
	{
		return true;
	}
	return false;
}

///Definition of virtual Game method getSquare().  This method gets valid input for both
///the coordinates of the Square on which the Piece currently is resting, and the 
///coordinates of the Square to which the piece will be moved. 
Square* Chess::getSquare(std::istream &is) {	
	int x, y;
	is >> x >> y;

	while(is.fail())
	{
		is.clear();
		is.ignore(256, '\n');
		std::cout << "You may not enter strings, characters, or anything other than intergers."  << std::endl;
	
		is >> x >> y;
	}

	while(x<0 || x>5 || y<0 || y>5)
	{
		is.clear();
		is.ignore(256, '\n');
		std::cout << "Invalid coordinates.  Coordinates must be integers from 0-5." << std::endl;
		is >> x >> y;
	}
	
	c->row = x;
	c->column = y;
	return gameBoard->getSquare(c);
}




///Returns true if the first location is valid based on whose turn it is (int i).
bool Chess::validFirstLocation(int i, Coord* c)
{
	if(gameBoard->getSquare(c)->getPiece()==NULL)
	{
		std::cout << "This square does not contain one of your pieces." << std::endl;
		return false;
	}

	if((gameBoard->getSquare(c)->getPiece()->color == "white") && (i%2 == 0))
		return true;
	if((gameBoard->getSquare(c)->getPiece()->color == "black") && (i%2 == 1))
		return true; 

	std::cout << "This square does not contain one of your pieces." << std::endl;
	return false;
}

///Takes a vector of coordinates and returns a null pointer unless a legal move
///exists in the vector. It checks each square/coordinate in the vector to see
///if a piece exists there, and if it does, it checks that the colour of that 
///piece is different from that of the Square* s it was given.  This function
///returns null if the orignal piece attempts to jump another piece.
Coord* Chess::legal(std::vector<Coord*> v, Square* s)
{
	if(v.empty())
		return NULL;


	for(int i=0; i<v.size(); i++)
	{
		if(gameBoard->getSquare(v[i])->getPiece() != NULL)
		{
			if(gameBoard->getSquare(v[i])->getPiece()->color != s->getPiece()->color)
			{
				if(i== v.size()-1)
					return v[i];
				else 
					return NULL;
			}

			
			else
				return NULL;
		}
		
		if(i== v.size()-1)
		{
			return v[i];
		}
	}
}



///Definition of virtual Game destructor.
Chess::~Chess()
{
///Deletes gameBoard, which calls the Board class destructor, deleting each 
///Square individually.
	delete gameBoard;
	delete c;

///Delete each unique Chess Piece individually	
	delete wp1;
	delete wp2;
	delete wp3;
	delete wp4;
	delete wp5;
	delete wp6;

	delete wr1;
	delete wb1;
	delete wk1;
	delete wq1;
	delete wb2;
	delete wr2;

	delete bp1;
	delete bp2;
	delete bp3;
	delete bp4;
	delete bp5;
	delete bp6;

	delete br1;
	delete bb1;
	delete bk1;
	delete bq1;
	delete bb2;
	delete br2;
}
