///Created by Lucas Jakober
///Created September 28, 2015
///Main file for a Chess Game.  


#include "Chess.h"
#include <iostream>

using namespace std;



int main(){

///Create the new Chess object on the heap, just for fun
	Chess *c = new Chess;

///Call the inherited function play() from Game, which in turn
///calls the virtual functions which are then defined in Chess class
	c->play();
	
	cout << "Thanks for playing!" << endl;

///Delete the Chess object off of the heap
	delete c;

   return 0;
}
   


